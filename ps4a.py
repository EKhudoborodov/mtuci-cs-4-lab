import itertools
# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.  

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    if len(sequence) == 0:
        print("String is empty!")
        pass
    elif len(sequence) == 1:
        return sequence
    else:
        same = count_same_letters(sequence)
        if same == 1:
            combinations = [sequence[0]+sequence[1], sequence[1]+sequence[0]]
            if len(sequence) == 2:
                return combinations
        elif (len(sequence)-same) == 0:
            return sequence
        else:
            sequence = rewrite_sequence(sequence, find_same_letters(sequence))
            combinations = [sequence[0]+sequence[1]]
        letters = []
        comb = ''
        for char in sequence:
            letters.append(char)
        mem = []
        for i in range(2, len(sequence)):
            for j in range(i+1):
                comb += letters[j]
            for pos in range(i+1):
                for k in range(len(combinations)):
                    if pos == 0:
                        a = comb[i]+combinations[k]
                    elif pos == i:
                        a = combinations[k] + comb[i]
                    else:
                        a = combinations[k][0:pos] + comb[i] + combinations[k][pos:len(letters)-1]
                    mem.append(a)
            combinations = mem
            comb, mem = [], []
        combinations = remove_same_elements(combinations)
        return combinations
def calculate_factorial(n):
    num = 1
    for i in range(n):
        num = num * (n - i)
    return num
def count_same_letters(sequence):
    same = 1
    for i in range(len(sequence)):
        if sequence.count(sequence[i]) > same:
            same = sequence.count(sequence[i])
    #print(same)
    return same

def find_same_letters(sequence):
    same = 1
    for i in range(len(sequence)):
        if sequence.count(sequence[i]) > same:
            same = sequence.count(sequence[i])
            sim = sequence[i]
    return sim
def rewrite_sequence(sequnce, sim):
    new_sequence = ''
    for char in sequnce:
        if char == sim:
            new_sequence += char
    for char in sequnce:
        if char != sim:
            new_sequence += char
    return new_sequence

def remove_same_elements(combinations):
    mem = combinations
    n = 0
    for i in range(len(mem)):
        if mem.count(mem[i-n]) > 1:
            combinations.remove(combinations[i-n])
            n += 1
    return combinations




def test_me(sequence):
    my_list = get_permutations(sequence)
    their_list = [''.join(word) for word in set(itertools.permutations(sequence))]

    print(f'{sequence} - input string')
    print(f'{my_list} - my answer')
    print(f'{their_list} - correct answer ')


if __name__ == '__main__':
    test_me('aaa')
    test_me('abcde')
    test_me('aabb')


